from flask import Flask 
app = Flask(__name__)

@app.route("/")
def hello_word():
    return("liyin is craving brownies!")

@app.route("/courses")
def courses():
    return ("No courses available, sorry!")

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
